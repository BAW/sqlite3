import { Component, OnInit } from '@angular/core';
import { SQLiteService } from '../services/sqlite.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(private _sqlite: SQLiteService) {}
  ngOnInit() {

  }

  async runTest(): Promise<void> {
    try {
      let result: any = await this._sqlite.echo("Hello World");
      console.log(" from Echo " + result.value);

      // ************************************************
      // Create Database No Encryption
      // ************************************************

      // initialize the connection
      let db = await this._sqlite
                  .createConnection("testEncryption", false, "no-encryption", 1);

      // open db testEncryption
      await db.open();

      await this._sqlite.closeConnection("testEncryption"); 

      return Promise.resolve();

    } catch (err) {
      return Promise.reject(err);
    }
  }

  async click() {
    console.log('HERE');
    try {
      await this.runTest();
    } catch (err) {
      console.log(`$$$ runTest failed ${err.message}`);
    }
  }
}
